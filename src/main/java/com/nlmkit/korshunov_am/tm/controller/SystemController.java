package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.TerminalConst.TASK_LISTS_BY_PROJECT_ID;

/**
 * Контроллер системных комманд
 */
public class SystemController extends AbstractController {
    /**
     * Конструктор
     * @param сommandHistoryService сервис истории комманд
     */
    public SystemController(CommandHistoryService сommandHistoryService) {
        super(сommandHistoryService);
    }

    /**
     * Показать выход
     * @return 0 выполнено
     */
    public int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }

    /**
     * Показать что была ошибка
     * @return -1 ошибка
     */
    public int displayError(){
        System.out.println("Error! Unknown command.");
        return -1;
    }

    /**
     * Приглашение при входе в программу
     */
    public void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        System.out.println("Enter command");
    }

    /**
     * Показать версию
     * @return 0 выполнено
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        System.out.println("Short command name in brackets");
        System.out.println("----Common commands:");
        System.out.println("version - Display version (v)");
        System.out.println("about - Display developer info (a)");
        System.out.println("help - Display list of command (h)");
        System.out.println("exit - Terminate console application (e)");
        System.out.println("----Project commands:");
        System.out.println("project-create - Create new project by name. (pcr)");
        System.out.println("project-clear - Remove all projects. (pcl)");
        System.out.println("project-list - Display list of projects. (pl)");
        System.out.println("project-view - View project info. (pv)");
        System.out.println("project-remove-by-id - Remove project by id. (prid)");
        System.out.println("project-remove-by-name - Remove project by name. (prn)");
        System.out.println("project-remove-by-index - Remove project by index. (prin)");
        System.out.println("project-update-by-index - Update name and description of project by index. (puin)");
        System.out.println("----Task commands:");
        System.out.println("task-create - Create new task by name. (tcr)");
        System.out.println("task-clear - Remove all tasks. (tcl)");
        System.out.println("task-list - Display list of tasks. (tl)");
        System.out.println("task-view - View task info. (tv)");
        System.out.println("task-remove-by-id - Remove task by id. (trid)");
        System.out.println("task-remove-by-name - Remove task by name. (trn)");
        System.out.println("task-remove-by-index - Remove task by index. (trin)");
        System.out.println("task-update-by-index - Update name and description of task by index. (tuin)");
        System.out.println("----Task in project commands:");
        System.out.println("task-list-by-project-id - Display task list by project if. (tlp)");
        System.out.println("task-add-to-project-by-ids - Add task to project by id. (tap)");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by id. (trp)");
        System.out.println("----User commands:");
        System.out.println("user-create - Create user. (uc)");
        System.out.println("user-list - View list of users. (ul)");
        System.out.println("user-view-by-id - View user data by id. (uvid)");
        System.out.println("user-view-by-index - View user data by index. (uvi)");
        System.out.println("user-view-by-login - View user data by login. (uvl)");
        System.out.println("user-remove-by-id - Remove user by id. (urid)");
        System.out.println("user-remove-by-index - Remove user by index. (uri)");
        System.out.println("user-remove-by-login - Remove user by login. (url)");
        System.out.println("user-update-by-id - Update user data by id. (uuid)");
        System.out.println("user-update-by-index - Update user data by index. (uui)");
        System.out.println("user-update-by-login - Update user data by login. (uul)");
        System.out.println("user-update-password-by-id - Change password of user by id. (uupid)");
        System.out.println("user-update-password-by-index - Change password of user by index. (uupi)");
        System.out.println("user-update-password-by-login - Change password of user by login. (uupl)");
        System.out.println("user-auth - Auth user. (ua)");
        System.out.println("user-update-password - Update password of auth user. (uup)");
        System.out.println("user-view - View auth user data. (uv)");
        System.out.println("user-update - Update auth user data. (uu)");
        System.out.println("user-end-session - End auth user session. (ue)");
        System.out.println("user-of-project-set-by-index - Set user of project. Project find by index (up)");
        System.out.println("user-of-task-set-by-index - Set user of task. Task find by index. (ut)");
        System.out.println("command-history-view - View command history. (chv)");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать информацию о разработчике
     * @return
     */
    public int displayAbout() {
        System.out.println("Andrey Korshunov");
        System.out.println("korshunov_am@nlmk.com");
        ShowResult("[OK]");
        return 0;
    }

}
