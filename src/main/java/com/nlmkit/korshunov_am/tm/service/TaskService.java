package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задач
 */
public class TaskService {
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Конструтор
     * @param taskRepostory Репозитарий задач
     */
    public TaskService(final TaskRepostory taskRepostory) {
        this.taskRepostory = taskRepostory;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.create(name,userId);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name, final String description,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepostory.create(name, description, userId);
    }

    /**
     * Измениь задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id, final String name, final String description,final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepostory.update(id, name, description, userId);
    }

    /**
     * Удалить все задачи
     */
    public void clear() {
        taskRepostory.clear();
    }

    /**
     * Удалить все задачи пользователя
     */
    public void clear(final Long userId ) {
        taskRepostory.clear(userId);
    }

    /**
     * Найти по индексу
     * @param index индекс
     * @return задача
     */
    public Task findByIndex(final int index) {
        if (index < 0) return null;
        return taskRepostory.findByIndex(index);
    }

    /**
     * Найти по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final int index,final Long userId) {
        if (userId == null) return null;
        if (index < 0) return null;
        return taskRepostory.findByIndex(index,userId);
    }

    /**
     * Найти по имени
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.findByName(name);
    }

    /**
     * Найти по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByName(final String name,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.findByName(name);
    }

    /**
     * Найти по идентификатору
     * @param id идентификатор
     * @return задча
     */
    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepostory.findById(id);
    }

    /**
     * Найти по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задча
     */
    public Task findById(final Long id,final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepostory.findById(id,userId);
    }

    /**
     * Удалить по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index) {
        if (index < 0) return null;
        return taskRepostory.removeByIndex(index);
    }

    /**
     * Удалить по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final int index,final Long userId) {
        if (userId == null) return null;
        if (index < 0) return null;
        return taskRepostory.removeByIndex(index,userId);
    }

    /**
     * Удалить по идентификаотру
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepostory.removeById(id);
    }

    /**
     * Удалить по идентификаотру и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepostory.removeById(id,userId);
    }

    /**
     * Удалить по имени
     * @param name имя
     * @return задача
     */
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.removeByName(name);
    }

    /**
     * Удалить по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByName(final String name,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepostory.removeByName(name,userId);
    }

    /**
     * Получить все задачи проекта
     * @param projectId
     * @return список задач
     */
    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить все задачи проекта с учетом пользователя
     * @param projectId
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) {
        if (userId == null) return null;
        if (projectId == null) return null;
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Получить все задачи
     * @return список задач
     */
    public List<Task> findAll() {
        return taskRepostory.findAll();
    }

    /**
     * Получить все задачи пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) {
        return taskRepostory.findAll(userId);
    }

    /**
     * Получить задачу по проекту и ид
     * @param projectId ид проекта
     * @param id ид
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepostory.findByProjectIdAndId(projectId, id);
    }

    /**
     * Получить задачу по проекту ид и пользователю.
     * @param projectId ид проекта
     * @param id ид
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id, final Long userId) {
        if (userId == null) return null;
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepostory.findByProjectIdAndId(projectId, id, userId);
    }

}
